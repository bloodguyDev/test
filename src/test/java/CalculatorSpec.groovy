import spock.lang.Specification

class CalculatorSpec extends Specification {

    def calculator = new Calculator()

    def "should return square of numbers smaller than 64"() {
        given:
        def numbers = [-9, -5, -1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100]

        when:
        def result = calculator.calculateSquareOfNumbers(numbers)

        then:
        result == [25, 1, 4, 9, 16, 25, 36, 49]
    }
}
