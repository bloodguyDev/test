import java.util.List;
import java.util.stream.Collectors;

public class Calculator {

    public List calculateSquareOfNumbers(List<Integer> numbers) {
        return numbers.stream()
                .map(num -> Math.pow(num, 2))
                .filter(num -> num < 64)
                .collect(Collectors.toList());
    }

}
